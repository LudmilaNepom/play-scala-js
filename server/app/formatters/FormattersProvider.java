package formatters;

import models.entities.Department;
import models.entities.Person;
import models.repository.department.DepartmentRepository;
import models.repository.person.PersonRepository;
import play.data.format.Formatters;
import play.i18n.MessagesApi;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

@Singleton
public class FormattersProvider implements Provider<Formatters> {
    private final MessagesApi messagesApi;
    private final DepartmentRepository departmentRepository;
    private final PersonRepository personRepository;

    @Inject
    public FormattersProvider(MessagesApi messagesApi, DepartmentRepository departmentRepository,
                              PersonRepository personRepository) {
        this.messagesApi = messagesApi;
        this.departmentRepository = departmentRepository;
        this.personRepository = personRepository;
    }

    @Override
    public Formatters get() {
        Formatters formatters = new Formatters(messagesApi);
        formatters.register(Department.class, new DepartmentFormatter(departmentRepository));
        formatters.register(Person.class, new PersonFormatter(personRepository));
        return formatters;
    }
}

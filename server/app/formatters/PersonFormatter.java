package formatters;

import com.google.inject.Inject;
import models.entities.Person;
import models.repository.person.PersonRepository;
import play.data.format.Formatters;

import java.text.ParseException;
import java.util.Locale;

public class PersonFormatter extends Formatters.SimpleFormatter<Person> {
    private final PersonRepository personRepository;


    @Inject
    public PersonFormatter(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Person parse(String input, Locale l) throws ParseException {
        return personRepository.find(Long.valueOf(input));
    }

    @Override
    public String print(Person department, Locale l) {
        return Long.toString(department.getId());
    }

}

package controllers;

import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class HomeController extends Controller {

    public Result index() {
        return ok(views.html.home.index.render());
    }

    public Result getArray() {
        ArrayList<Integer> numbers = new ArrayList<>();
        IntStream.range(0, 6).forEach(numbers::add);
        return ok(views.html.home.array.render(numbers));
    }

    public Result changeLanguage(String lang) {
        ctx().changeLang(lang);
        return ok();
    }


}

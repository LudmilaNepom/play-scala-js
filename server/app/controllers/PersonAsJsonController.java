package controllers;

import com.google.inject.Inject;
import models.entities.Department;
import models.entities.Person;
import models.repository.department.DepartmentRepository;
import models.repository.person.PersonRepository;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

public class PersonAsJsonController extends Controller {
    private final PersonRepository personRepository;
    private final DepartmentRepository departmentRepository;
    private final FormFactory formFactory;
    private MessagesApi messagesApi;
    private Form<Person> form;

    @Inject
    public PersonAsJsonController(PersonRepository personRepository, FormFactory formFactory, MessagesApi messagesApi,
                                  DepartmentRepository departmentRepository) {
        this.personRepository = personRepository;
        this.formFactory = formFactory;
        this.messagesApi = messagesApi;
        this.departmentRepository = departmentRepository;
    }

    /*

    edit action -> display edit form
    doEdit action -> handle submit form and return json (error or success)

     */


    @Transactional
    public Result edit(int id) {
        Person person;
        try {
            person = personRepository.find((long) id);
        } catch (Exception e) {
            return badRequest("Person with such id does not exist");
        }

        Form<Person> form = formFactory.form(Person.class).fill(person);
        List<Department> departments = departmentRepository.getAll();
        return ok(views.html.person.asjson.editpersonwithjson.render(form, departments, null));

    }

    @Transactional
    public Result doEdit(int id) {
        Person person = personRepository.find((long) id);
        Form<Person> form = formFactory.form(Person.class).fill(person).bindFromRequest();
        if (form.hasErrors()) {
            flash("error", messagesApi.get(lang(), "msg.form.error"));
            return badRequest(form.errorsAsJson());
        }
        return redirect(routes.PersonAsJsonController.edit(id));
    }

}

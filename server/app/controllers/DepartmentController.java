package controllers;


import com.google.inject.Inject;
import models.entities.Department;
import models.entities.Person;
import models.repository.department.DepartmentRepository;
import models.repository.person.PersonRepository;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.i18n.Lang;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

public class DepartmentController extends Controller {
    private final DepartmentRepository departmentRepository;
    private final PersonRepository personRepository;
    private final FormFactory formFactory;
    private MessagesApi messagesApi;

    @Inject
    public DepartmentController(DepartmentRepository departmentRepository, PersonRepository personRepository,
                                FormFactory formFactory, MessagesApi messagesApi) {
        this.departmentRepository = departmentRepository;
        this.personRepository = personRepository;
        this.formFactory = formFactory;
        this.messagesApi = messagesApi;
    }

    @Transactional
    public Result list() {
        List<Department> departments = departmentRepository.getAll();
        return ok(views.html.department.list.render(departments));
    }

    public Result add() {
        Form<Department> departmentForm = formFactory.form(Department.class);
        return ok(views.html.department.add.render(null, departmentForm));
    }

    @Transactional
    public Result doAdd() {
        Form<Department> form = formFactory.form(Department.class).bindFromRequest();
        if (form.hasErrors()) {
            flash("error", messagesApi.get(Lang.forCode("ru"), "msg.form.error"));
            return badRequest(views.html.department.add.render(null, form));
        }
        departmentRepository.save(form.get());
        flash("success", messagesApi.get(lang(), "msg.department.add.success"));
        return redirect(routes.DepartmentController.list());
    }

    @Transactional
    public Result edit(Long id) {
        Department department = departmentRepository.find(id);
        Form<Department> departmentForm = formFactory.form(Department.class).fill(department);
        department.getPersons();
        return ok(views.html.department.edit.render(department, departmentForm, department.getPersons()));
    }

    @Transactional
    public Result doEdit(Long id) {

        Department department = departmentRepository.find(id);
        Form<Department> form = formFactory.form(Department.class).bindFromRequest();

        if (form.hasErrors()) {
            flash("error", messagesApi.get(Lang.forCode("ru"), "msg.form.error"));
            return badRequest(views.html.department.edit.render(department, form, department.getPersons()));
        }
        Department editDepartment = form.get();
        editDepartment.setId(department.getId());
        editDepartment.setPersons(departmentRepository.find(id).getPersons());
        departmentRepository.save(editDepartment);
        flash("success", messagesApi.get(lang(), "msg.department.edit.success"));
        return redirect(routes.DepartmentController.list());
    }

    @Transactional
    public Result delete(Long id) {
        departmentRepository.delete(id);
        flash("success", messagesApi.get(lang(), "msg.department.delete.success"));
        return redirect(routes.DepartmentController.list());
    }

    @Transactional
    public Result deletePersonFromDepartment(Long departmentId, Long personId) {
        Department department = departmentRepository.find(departmentId);
        Person person = personRepository.find(personId);
        person.removeDepartment(department);
        department.removePerson(person);
        return redirect(routes.DepartmentController.edit(departmentId));
    }

}

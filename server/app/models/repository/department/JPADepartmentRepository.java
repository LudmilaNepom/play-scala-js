package models.repository.department;

import models.entities.Department;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class JPADepartmentRepository implements DepartmentRepository {
    final private JPAApi jpaApi;

    @Inject
    public JPADepartmentRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    @Override
    public Department save(Department department) {
        EntityManager em = jpaApi.em();
        return em.merge(department);
    }

    @Override
    public Department find(Long id) {
        EntityManager em = jpaApi.em();
        TypedQuery<Department> namedQuery = em.createNamedQuery("Department.findById", Department.class).setParameter("id", id);
        return namedQuery.getSingleResult();
    }

    @Override
    public void delete(Long id) {
        EntityManager em = jpaApi.em();
        TypedQuery<Department> namedQuery = em.createNamedQuery("Department.findById", Department.class).setParameter("id", id);
        em.remove(namedQuery.getSingleResult());
    }

    @Override
    public List<Department> getAll() {
        EntityManager em = jpaApi.em();
        TypedQuery<Department> namedQuery = em.createNamedQuery("Department.getAll", Department.class);
        return namedQuery.getResultList();
    }
}

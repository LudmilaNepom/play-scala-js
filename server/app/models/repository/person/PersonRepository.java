package models.repository.person;

import com.google.inject.ImplementedBy;
import models.entities.Person;

import java.util.List;

@ImplementedBy(JPAPersonRepository.class)
public interface PersonRepository {
    Person save(Person person);

    Person find(Long id);

    void delete(Long id);

    List<Person> getAll();

    List<Person> getPage(int page, int itemsPerPage);

    int getCountOfPages(int itemsPerPage);

    List<Person> find(String search);
}

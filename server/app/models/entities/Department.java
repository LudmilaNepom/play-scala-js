package models.entities;

import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "insign_department")
@NamedQueries({
        @NamedQuery(name = "Department.getAll", query = "SELECT d FROM Department d"),
        @NamedQuery(name = "Department.findById", query = "SELECT d FROM Department d WHERE d.id=:id")
})
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Constraints.Required
    @Constraints.MaxLength(value = 15)
    @Constraints.Pattern(value = "^[0-9a-zA-Zа-яёА-ЯЁ\\s]+$", message = "pattern.department.name.error")
    private String name;
    @ManyToMany(mappedBy = "departments")
    private List<Person> persons = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void removePerson(Person person) {
        persons.remove(person);
    }

    public void addPerson(Person person) {
        persons.remove(person);
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

}

msg.invalid.person.email=Не правильный аддрес
msg.form.error=Ошибка! Пожалуйста отредактируйте форму ниже
msg.person.add.success=Сотрудник был успешно добавлен
msg.person.edit.success=Сотрудник был успешно отредактирован
msg.person.delete.success=Сотрудник был успешно удален
msg.department.add.success=Отдел был успешно добавлен
msg.department.edit.success=Отдел был успешно отредактирован
msg.department.delete.success=Отдел был успешно удален
pattern.department.name.error=Может содержать только цифры, английские буквы и пробелы
page.home.overview=Обзор
page.home.persons=Сотрудники
page.home.departments=Отделы
page.home.arrays=Массивы
page.home.homepage=Домашняя страница
page.home.project.name=Тренировочный проект
page.home.welcome=Добро пожаловать в Play
page.home.language=Изменить язык:
page.home.language.ru=рус
page.home.language.en=анг
page.home.add.new=Добавить
$('#toTop').click(function () {
    $("html, body").animate({scrollTop: 0}, 600);
    return false;
});

$(document).ready(function () {

    $(document).on("click", '.ajaxPageUpdate', function (event) {

        var url = $(this).attr('href');


        $("#updateable").load(url);

        event.preventDefault();
        event.stopPropagation();
    });

});

$(document).ready(function () {

    $(document).on("click", '.navbar-right .lang', function (event) {
        var url = $(this).attr('href');

        $.get(url);
        event.preventDefault();
        event.stopPropagation();
        location.href = location.href;
    });

});






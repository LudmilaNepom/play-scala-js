package example

import org.scalajs.dom
import dom.{Attr, Event, XMLHttpRequest, document, window}

import scala.scalajs.js
import org.querki.jquery._
import org.scalajs.dom.raw.HTMLElement

import scala.scalajs.js.annotation.JSBracketAccess
import example._

import scala.scalajs.js.UndefOr

object ScalaJSExample extends js.JSApp {
    implicit def jq2AjaxForm(jq:JQuery):AjaxForm = jq.asInstanceOf[AjaxForm]

    def main(): Unit = {
      $(() => formOnSubmit())
    }
    def formOnSubmit(): Unit = {
      val $form: JQuery = $("#workWithJson")
//      val urlSend:String = $form.attr("action").asInstanceOf[String]
      $("#workWithJson").ajaxForm(AjaxSubmitOptions
        .beforeSubmit({ (arr: js.Any, form: org.querki.jquery.JQuery, options: js.Any) => {
          $(".form-group.has-error").remove()
          true
        }})
//        .data(js.Dictionary("add" -> "name"))
//        .url(urlSend.dropRight(4) + "1401")
//        .success({(responseText: js.Any, statusText: String, xhr: JQueryXHR, form: org.querki.jquery.JQuery)=> {
//          println(statusText)
//          println(xhr.getAllResponseHeaders())
//        }
//        })
        .error({(xhr:org.querki.jquery.JQueryXHR) =>{
          $(".form-group.has-error").remove()
          val errors = js.JSON.parse(xhr.responseText)
          val field1 = errors.firstName
          if (!field1.isInstanceOf[Unit] ){
            $form
              .find("input[name=\"firstName\"]")
              .after("<div class=\"form-group has-error\"><span class=\"help-block\">" + field1 + "</span></div>")
          }
          val field2 = errors.surname
          if (!field2.isInstanceOf[Unit] ){
            $form
              .find("input[name=\"surname\"]")
              .after("<div class=\"form-group has-error\"><span class=\"help-block\">" + field2 + "</span></div>")
          }
          val field3 = errors.email
          if (!field3.isInstanceOf[Unit] ){
            $form
              .find("input[name=\"email\"]")
              .after("<div class=\"form-group has-error\"><span class=\"help-block\">" + field3 + "</span></div>")
          }
        }})
//        .clearForm(true)
      )
    }
}








name := "example.js"
organization := "ua.mk"
version := "1.0-SNAPSHOT"

val scalaV = "2.11.11"
// "2.11.8"

lazy val server = (project in file("server")).settings(
  scalaVersion := scalaV,
  scalaJSProjects := Seq(client),
  pipelineStages in Assets := Seq(scalaJSPipeline),
  pipelineStages := Seq(digest, gzip),
  // triggers scalaJSPipeline when using compile or continuous compilation
  compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
  libraryDependencies ++= Seq(
    javaJpa,
    filters,
    "mysql" % "mysql-connector-java" % "5.1.36",
    "org.eclipse.persistence" % "eclipselink" % "2.4.2",
    "com.vmunier" %% "scalajs-scripts" % "1.0.0",
    "org.webjars" %% "webjars-play" % "2.5.0",
    "org.webjars" % "bootstrap" % "3.1.1-2",
    "org.webjars.bower" % "jquery" % "3.2.1",
    "com.adrianhurt" % "play-bootstrap_2.11" % "1.0-P25-B3",
    specs2 % Test
  )
).enablePlugins(PlayJava).
  dependsOn(sharedJvm)

lazy val client = (project in file("client")).settings(
  scalaVersion := scalaV,
  scalaJSUseMainModuleInitializer := true,
  libraryDependencies ++= Seq(
    "org.scala-js" %%% "scalajs-dom" % "0.9.0",
    "org.scala-js" %%% "scalajs-tools" % "0.6.6",
    "org.querki" %%% "jquery-facade" % "1.0-RC3",
    "org.querki" %%% "querki-jsext" % "0.8"
  ),
  jsDependencies ++= Seq(
    "org.webjars" % "jquery" % "1.10.2" / "jquery.js" minified "jquery.min.js",
    "org.webjars" % "bootstrap" % "3.1.1-2" / "bootstrap.js" minified "bootstrap.min.js" dependsOn "jquery.js",
    "org.webjars" % "log4javascript" % "1.4.10" / "js/log4javascript_uncompressed.js" minified "js/log4javascript.js",
    "org.webjars" % "FlexSlider" % "2.2.2" / "jquery.flexslider.js" minified "jquery.flexslider-min.js",
    "org.webjars" % "jquery-form" % "3.51" / "jquery.form.js",
    "org.webjars" % "jquery-ui" % "1.10.3" / "ui/jquery-ui.js" minified "ui/minified/jquery-ui.min.js"
  )
).enablePlugins(ScalaJSPlugin, ScalaJSWeb).
  dependsOn(sharedJs)

lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared")).
  settings(scalaVersion := scalaV).
  jsConfigure(_ enablePlugins ScalaJSWeb)

lazy val sharedJvm = shared.jvm
lazy val sharedJs = shared.js

// loads the server project at sbt startup
onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value








